package com.zimug.courses.security.basic.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zimug.commons.exception.AjaxResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Auther zxj
 * @Date 2021/1/12 14:30
 * @Describe :
 */
@Component
public class MyAuthenticationSuccessHandler
        extends SavedRequestAwareAuthenticationSuccessHandler {

    //在application配置文件中配置登陆的类型是JSON数据响应还是做页面响应
    @Value("${spring.security.logintype}")
    private String loginType;
    //Jackson JSON数据处理类
    private  static ObjectMapper objectMapper = new ObjectMapper();


    //页面跳转的处理逻辑
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication)
            throws ServletException, IOException {

        if (loginType.equalsIgnoreCase("JSON")) {
            // 前后端分离的项目返回json格式的字符串（用户信息？），具体的跳转由前端决定？
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(objectMapper.writeValueAsString(AjaxResponse.success()));
        } else {
            // 我们自定义的MyAuthenticationSuccessHandler之所以没有直接实现 AuthenticationSuccessHandler
            // 而是选择继承SavedRequestAwareAuthenticationSuccessHandler类（该类实现了AuthenticationSuccessHandler）
            // 就是因为SavedRequestAwareAuthenticationSuccessHandler 会记住用户上一次请求的地址，登录成功后直接跳转该地址

            // 前后端不分离的项目？会帮我们跳转到上一次请求的页面上
            //super.onAuthenticationSuccess(request, response, authentication);
            // 前后端不分离的项目？跳转到/index.html
            redirectStrategy.sendRedirect(request, response, "/index.html");

        }
    }
}
