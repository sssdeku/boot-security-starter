package com.zimug.courses.security.basic.config;

import com.zimug.courses.security.basic.handler.CustomExpiredSessionStrategy;
import com.zimug.courses.security.basic.handler.MyAuthenticationFailureHandler;
import com.zimug.courses.security.basic.handler.MyAuthenticationSuccessHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @Auther zxj
 * @Date 2021/1/12 11:17
 * @Describe :
 */
@Configuration
@RequiredArgsConstructor
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {


//    @Autowired
    private final MyAuthenticationFailureHandler myAuthenticationFailureHandler;
//    @Autowired
    private final MyAuthenticationSuccessHandler myAuthenticationSuccessHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable() //禁用跨站csrf攻击防御，后面的章节会专门讲解
                .formLogin()
                .loginPage("/login2.html")//一旦用户的请求没有权限就跳转到这个页面
                .loginProcessingUrl("/login")//登录表单form中action的地址，也就是处理认证请求的路径
                .usernameParameter("username")///登录表单form中用户名输入框input的name名，不修改的话默认是username
                .passwordParameter("password")//form中密码输入框input的name名，不修改的话默认是password
//                .defaultSuccessUrl("/")//登录认证成功后默认转跳的路径
//                .failureForwardUrl("/login.html")//登录失败的默认跳转路径
                //注意：这两个handler和上面的跳转路径不共存
                .failureHandler(myAuthenticationFailureHandler)//登录失败的处理逻辑
                .successHandler(myAuthenticationSuccessHandler)//登录成功的处理逻辑
                .and()
                .authorizeRequests()
                .antMatchers("/login.html","/login","/invalidSession.html","/login2.html").permitAll()//不需要通过登录验证就可以被访问的资源路径
                .antMatchers("/","/biz1","/biz2") //资源路径匹配
                .hasAnyAuthority("ROLE_user","ROLE_admin")  //user角色和admin角色都可以访问
                .antMatchers("/syslog","/sysuser")  //资源路径匹配
                .hasAnyRole("admin")  //admin角色可以访问
                //.antMatchers("/syslog").hasAuthority("sys:log")
                //.antMatchers("/sysuser").hasAuthority("sys:user")
                .anyRequest().authenticated()
                //
                .and()
                //session创建策略：
                // always：如果当前请求没有对应的session存在，Spring Security创建一个session。
                //ifRequired（默认）： Spring Security在需要使用到session时才创建session
                //never： Spring Security将永远不会主动创建session，但是如果session在当前应用中已经存在，它将使用该session
                //stateless：Spring Security不会创建或使用任何session。适合于接口型的无状态应用（前后端分离无状态应用），这种方式节省内存资源
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
//                .invalidSessionUrl("/invalidSession.html")//非法超时session跳转页面
                .maximumSessions(1)//表示同一个用户最大的登录数量
                .maxSessionsPreventsLogin(true)//true表示已经登录就不予许再次登录， false表示允许再次登录但是之前的登录账户会被踢下线
                //自定义一个session被下线(超时)之后的处理策略。注意：当配置有invalidSessionUrl：非法超时session跳转页面时，会直接跳转到invalidSessionUrl
                .expiredSessionStrategy(new CustomExpiredSessionStrategy());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user")
                .password(passwordEncoder().encode("123456"))
                .roles("user")
                .and()
                .withUser("admin")
                .password(passwordEncoder().encode("123456"))
                .authorities("sys:log","sys:user")
                .roles("admin")
                .and()
                .passwordEncoder(passwordEncoder());//配置BCrypt加密
    }

    @Override
    public void configure(WebSecurity web) {
        //将项目中静态资源路径开放出来
        web.ignoring().antMatchers( "/css/**", "/fonts/**", "/img/**", "/js/**");
    }
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
